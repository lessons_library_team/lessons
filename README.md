# Lessons

This set of lessons offers an easy way to learn the professional way to share code. 

In addition to this public site, we've provided a set of lessons in [Canvas](https://nwmissouri.instructure.com/courses/8431) to use in the classroom.

![Code sharing learning paths](resources/git_lesson_roadmap.jpg "Code sharing learning paths")

&copy; 2017 All rights reserved Northwest Missouri State University

Denise M Case
Charles Hoot
Diana Linville


