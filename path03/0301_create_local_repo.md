# Create a Local Repository 

Path 03 - Lesson 01

## What You Will Need

To complete this lesson, you will need to have the following installed on your machine

- A tool that will let you create and edit plain text files.  
- A tool that will let you run Git commands.
- A tool that you will use to create/run your project



## Create your project.
- You will want to create an enclosing folder that you will put under version control.  Name it **myRepo**.

- In the myRepo folder you will create the file **README.md**.  This critical file is where you will give a basic description of the project and put instructions for installing/running it. Here is one that you can use for now [README template](./README.md)

- In the myRepo folder you will create the file **.gitignore**.  This file allows you to exempt certain files from version control.  Typically, these will be files or folders that are automatically generated or are pulled in from somewhere else. The contents of this file will depend on the tool that you use to create your project.
- Create the project folder **myProject**.  If you have initial versions of your project files, you can put them under this folder. 

## Create the local Repository
You are now going to create the structures that git will use to remember the history of your project.  It will be hidden in your repository folder with the name **.git**.

```
$git init
Initialized empty Git repository in /Users/rosie/myRepo/.git/
```

Check out the status of your repository.
```
$git status
On branch master

Initial commit

Untracked files:
(use "git add <file>..." to include in what will be committed)

.gitignore
README.md
myProject/

nothing added to commit but untracked files present (use "git add" to track)
```



[Onward to making your first commit](./0302.md)



* * *

#### When editing this page

Use [Dillinger](http://dillinger.io/) to test markdown. 

