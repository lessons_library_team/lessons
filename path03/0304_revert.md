# Reverting 

Path 03 - Lesson 04


## Explore your commit history

- Every time you commit, you have created a bit of history. The state of your working directory (aside from files that were not staged or have been ignored) was saved in your repository.  You can reach back and look at your commits and if needed, undo a commit.

- Use your git tool to look at the commits

```    
$git add filename master
```

## Revert 

Use your git tool to revert to your previous commit.  (Warning, you will loose any work done for that commit. It will be forgotten from the repo and the working directory will change back.) 

```
$git revert commit
```

## Check the status of your repository
```
$git status
```

- Take a screen shot of your commit




Link to next


* * *

#### When editing this page

Use [Dillinger](http://dillinger.io/) to test markdown. 
