# Modify Files and Commit to a Local Repository 

Path 03 - Lesson 03


## Modify a file

- Use your project tool to make a change to one of your files.
- Remember to save your changes.

## Check the status
You should see the file that you have changed listed out.  If not, go back and make sure it has been saved.
$git status
On branch master
Changes not staged for commit:
(use "git add <file>..." to update what will be committed)
(use "git checkout -- <file>..." to discard changes in working directory)

modified:   myProject/index.html
modified:   myProject/style.css

no changes added to commit (use "git add" and/or "git commit -a")

```

## Stage the file
When you do a commit, you specifically need to tell git which of the files that have changed should be added into the repository.  
    1 You can select the files using the git add command or 
    2 Use the -a flag on your commit to tell git to stage all the files that have been modified or deleted.

*Important note: The -a flag does not pick up new files. You have to explicitly add them.  

- Lets use the automatic option with commit


## Commit the changed content to the repository

- Use your git tool with the -a option stage all the changed files and then commit them to your repository.

```
$git -a commit -m "Second version with some changes"
```

## Check the status of your repository

```
$git status
```
## Look at the history of your repository




- Take a screen shot of your commit


## Repeat this process
Continue to make refinements (small changes) in your application files.  With each refinement save, stage, and commit. This is a basic component of a git workflow, so do this until you are comfortable with it.


Link to next


* * *

#### When editing this page

Use [Dillinger](http://dillinger.io/) to test markdown. 
