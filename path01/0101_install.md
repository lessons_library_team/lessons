# Installing Git 

Path 01 - Lesson 01

## Sharing Code

Professional software engineers need a way to manage and share code as they create, develop, and maintain their applications.

Systems designed to keep track of code from creation and throughout its development and maintenance are called version control systems (VCS).

The most popular VCS (and the one we'll use in these lessons) is Git. 

## Installing Git for Windows

- Download and run the executable from https://git-scm.com/download/win.

- Explore the other resources on the site. The free Apress (yellow and black) is an excellent reference for learning more about this important software.

- After installing, create a folder C:\lessons. 

- In File Explorer, go to C:\ and right-click on the lessons folder and select Git Bash Here. Type the Linux command ls in the Bash terminal. Take a screen shot of the results. 

## Install Tortoise Git

- Download and install [TortoiseGit](https://tortoisegit.org/), a handy utility that integrates with File Explorer.

## Enhance File Explorer Context Menu

- Enhance your Windows File Explorer context menu so you can right-click on a folder and 
["Open a Command Window Here as Administrator"](https://www.sevenforums.com/tutorials/47415-open-command-window-here-administrator.html).
 
    
## Installing Git for maintenance

- Download and run the executable from https://git-scm.com/download/mac.

## Set your Preferences for Opening Terminal

It will be useful to open a Terminal window on a particular folder.  You have to change your system perferences so that you have that service.

- Open System Preferences (You can find it under the apple)

- Open the Keyboard preferences and select Shortcuts

- Select "Services" on the left and check the box next to "New Terminal at Folder"

Now if you control-click on a folder in Finder, you will get a pop-up menu.  One of the options is Services and under that you will find New Terminal at Folder.  If you select this, a terminal window running bash will start up with that folder as the working directory

## Look at the Directory you Created

- Control-click on the lessons folder and get a new terminal at folder from Services.

- Type the command ls -al and take a screen shot of the results.

--
    
    
* * *

#### When editing this page

Use [Dillinger](http://dillinger.io/) to test markdown. 







