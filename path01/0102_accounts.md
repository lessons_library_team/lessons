# Creating Cloud Accounts to Store Code 

Path 01 - Lesson 02

A repository (aka repo) is a place to store code. 

There are many cloud hosting companies who allow you to host repositories in the cloud for free.
We'll start out by setting up accounts at two of the most well-known: BitBucket and GitHub.

## On BitBucket

- Go to https://bitbucket.org.

- Click on Get started.

- Create an account with your school email to get an academic plan with free unlimited private repositories. 





## On GitHub

- Go to https://github.com/.

- Fill in your information (use your school email) and sign up for GitHub. 


  
    
    
    
    
* * *

#### When editing this page

Use [Dillinger](http://dillinger.io/) to test markdown. 







