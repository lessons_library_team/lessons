# Cloning a Repository 

Path 02 - Lesson 02

- After Lesson 01, you have your own copy of the code - but it's in the cloud. 

- To modify and test the code, you'll want to get an exact copy of your cloud repo down to your local computer. 

- This is called "cloning" the repo and there are at least two good ways to do this on a Windows machine:

		1 Use Git Bash 
		2 Use TortoiseGit

## All options: Locate the source repo

- Open a browser. Go to the new BitBucket cloud repo you just created.  https://bitbucket.org/lessons_library_team/repo_0201_html_css.

- Verify the repo exists.

- Copy the entire source repo URL (from the starting https:// all the way to the ending repo_0201_html_css) to your clipboard (select the URL in a browser and use CTRL-C to copy). 

## Windows Option 1: Clone with Git Bash

 - In Windows File Explorer, go to C:\lessons.  This will be the parent folder. We want to clone the repo into this folder. 
 
 - Right click on the lessons folder and open Git Bash.  At the $ prompt, type the following command and hit ENTER. 
 
```
git clone https://bitbucket.org/YOUR-USERNAME/YOUR-REPOSITORY
```

## Windows Option 2: Clone with TortoiseGit

 - In Windows File Explorer, go to C:\lessons.  This will be the parent folder. We want to clone the repo into this folder. 
 
 - Right click on the lessons folder and from the context menu, select **Git clone...**
 
 - In the URL field, paste the source URL (https://bitbucket.org/YOUR-USERNAME/YOUR-REPOSITORY) and click OK.
 



## All options: Verify your new local repo

- Verify you have a C:\lessons\repo_0201_html_css folder with the code ready to modify and test. 
  
    
        
        
    
    
* * *

#### When editing this page

Use [Dillinger](http://dillinger.io/) to test markdown. 







