# Importing a BitBucket Repository 

Path 02 - Lesson 01

BitBucket makes it easy to grab code from another cloud repository. 

When you import, you create your own personal copy of the repo in your BitBucket cloud account.

Import is cloud to cloud (and has nothing to do with local code on your computer).



## Locate the source repo

- Open a browser. Go to https://bitbucket.org/lessons_library_team/repo_0201_html_css.

- Verify the repo exists.

- Copy the entire source repo URL to your clipboard (select the URL in a browser and use CTRL-C to copy). 



## Goto your BitBucket account and import

- In a browser, go to **your** BitBucket account on the web:  https://bitbucket.org/your_user_name_here. (For example, mine is https://bitbucket.org/professorcase/). 

- Click "Respositories" and "Import Repository" (it's the last item on the submenu).

- Paste the source repo URL (https://bitbucket.org/lessons_library_team/repo_0201_html_cssa) in the URL field. 

- It will default to the same name. This is perfect. 

- Uncheck "This is a private repository" if you would like to make your repository available to everyone on the World Wide Web.  Leave it checked to keep your repository private (visible only to you). 

- Click "Import repository". 



## Verify your new repo

- Verify you have a new repo in the cloud. 
  
    
    
    
    
* * *

#### When editing this page

Use [Dillinger](http://dillinger.io/) to test markdown. 







