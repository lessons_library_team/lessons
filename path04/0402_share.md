# Share a Repository 

Path 04 - Lesson 02

## Creating Your Local Copy of the Repository (Everyone)
- From the Overview of your repository, find its URL. 
![Image for repo URL](./images/0401RemoteRepoURL "Remote repo URL")
- Make a copy of the URL.  We will denote it later by <RemoteURL> and you should replace that by the copy.
- On your local machine, navigate to the directory where you will place your local version of the remote repository.
- Start a bash command line window. (Footnote basic methods)
- Clone the remote repository from its URL into a local directory with the name HTMLGit

|$git clone <RemoteURL> HTMLGit
|:---|
|Cloning into 'HTMLGit'...
|remote: Counting objects: 7, done.
|remote: Compressing objects: 100% (7/7), done.
|remote: Total 7 (delta 0), reused 0 (delta 0)
|Unpacking objects: 100% (7/7), done.|


- Change your current directory to the repository

|$cd HTMLGit
|:---|


- Check out the status of your repository.

|$git status
|:---|
|On branch master
|Your branch is up-to-date with 'origin/master'.
|nothing to commit, working tree clean




[Onward to making changes in the local repositories - Path 4 Lesson 3](./0403_contribute.md)
* * *

#### When editing this page

Use [Dillinger](http://dillinger.io/) to test markdown. 
