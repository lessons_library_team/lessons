# Further development in a team 

Path 04 - Lesson 05

## Do another round of revisions.
- Everyone pull the remote repository.
- Change index.html by displaying a fun fact after the image.
- View the changes in a browser to make sure they are what you want.
- Save the file. Commit the change. Push the commit.
- Resolve all merge conflicts.

## Things to explore
- If two people change a line in a file, will we get a merge conflict?
- If two people change different lines in a file, will we get a merge conflict?
- If two people take a chunk of lines and move the chunk to different locations, do we get a merge conflict?
- Do merge conflicts and their resolution work the same way for Microsoft .docx files?
- How does Git know if a file has changed?
- What kinds of files does Git treat as binary files? How does that affect the resolution process?
- How does diff affect the way Git works?
- What are the Git commands for resolving merge conflicts?
- How does Git identify the commits?

* * *

#### When editing this page

Use [Dillinger](http://dillinger.io/) to test markdown. 

