# Contribute to a local Repository 

Path 04 - Lesson 03

## Find the HTML file you will edit
- Inside the local repository you created should be a directory named **myProject**.

- Inside that directory should be three files:  index.html, style.css, and Rosie.jpg.

## Make a contribution
- Open index.html with a plain text editor.

- Just after the line <p>My name is Rosie</p> add in a paragraph of your own.
- Save the change you made and then look at it with a browser to make sure everything is good.  If not, keep editing until it looks right.

## Commit your change into the repository
- You should still have the command line window pointed at your repository from the previous lesson.  If not, open a new window at the repository (HTMLGit)

- Check the status.  You should see that there is a modification.

|$git status
|:---|
|On branch master
|Your branch is up-to-date with 'origin/master'.
|Changes not staged for commit:
|(use "git add <file>..." to update what will be committed)
|(use "git checkout -- <file>..." to discard changes in working directory)
|
|modified:   myProject/index.html
|
|no changes added to commit (use "git add" and/or "git commit -a")


- Stage (add) all the changes and commit. (Reminder: Your commit identifier will probably not match.) 

|$git commit -a -m "Added my name to index.html"
|:---|
|[master 28111f5] Added my name to index.html
|1 file changed, 1 insertion(+)

- Check the status again. Our local repository should be out of synch with the remote repository.

|$git status
|:---|
|On branch master
|Your branch is ahead of 'origin/master' by 1 commit.
|(use "git push" to publish your local commits)nothing to commit, working tree clean```

[Onward to pushing your work and resolving merge conflicts - Path 4 Lesson 4 ](./0404_pull_push_merge.md)

* * *

#### When editing this page

Use [Dillinger](http://dillinger.io/) to test markdown. 

