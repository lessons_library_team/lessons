# Pushing and Resolving Merge Conflicts 

Path 04 - Lesson 04

## Push your commit to the remote repository.

|$git push
|:---|

The result here depends on a race condition.  If you get there first, your push should go through and you should see something like:
```
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 466 bytes | 0 bytes/s, done.
Total 4 (delta 1), reused 0 (delta 0)
To https://bitbucket.org/charles_hoot/htmlgit.git
cbdc475..a004071  master -> master
```
If you get there after, you are likely to have a merge conflict and the push will fail:
```
To https://bitbucket.org/charles_hoot/htmlgit.git
! [rejected]        master -> master (fetch first)
error: failed to push some refs to 'https://charles_hoot@bitbucket.org/charles_hoot/htmlgit.git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```
If you got there before your team mates, your work is done for now. Do watch your team and see how they resolve the conflicts.  Later work with a team mate and make simultaneous changes. Let them win the push race so you have a chance to resolve a merge conflict too.

## Resolving the merge conflict.
- The first step in resolving a merge conflict is to pull down the changes. (The pull command does two things for you. (1) It does a **fetch** which brings down the remote version of branch you are working on.  (2) It does a **merge** of the two versions.)

|$git pull
|:---|
|remote: Counting objects: 4, done.
|remote: Compressing objects: 100% (4/4), done.
|remote: Total 4 (delta 1), reused 0 (delta 0)
|Unpacking objects: 100% (4/4), done.
|From https://bitbucket.org/charles_hoot/htmlgit
|cbdc475..a004071  master     -> origin/master
|Auto-merging myProject/index.html
|CONFLICT (content): Merge conflict in myProject/index.html
|Automatic merge failed; fix conflicts and then commit the result.

That is definitely a merge conflict.  It is now your responsibility to edit the indicated files, commit and then try to push again.

## Editing the merge conflict. 
Open the file myProject/index.html. You should see something like:
```        
<p>My name is Rosie</p>
<<<<<<< HEAD
<p>My name is Sammy</p>
=======
<p>My name is Johnson</p>
>>>>>>> a0040714a923f8e4d3707eff9ef58a845a17df98
```
The first chunk of the conflict is marked by <<<<<<<< HEAD and ========== and came from your version. The second chunk of the conflict is marked by =========== and >>>>>> a0040714a923f8e4d3707eff9ef58a845a17df98 and came from the remote version.

## Decide what to keep.
- You have the power to decide what stays and what goes. You can (1) Keep only your version (2) Keep only the remote version (3) Keep both (4) Keep parts of both (5) Keep nothing and create something completely new.
- Make the change. In our case, keep both!
- Delete the markers.
- Save the changes

## Do a local commit
|$git commit -a -m "Resolved merge conflict in myProject/index.html-Keeping both"
|:---|
|[master 9efaf81] Resolved merge conflict in myProject/index.html-Keeping both

- and check the status

|$git status
|:---|
|On branch master
|Your branch is ahead of 'origin/master' by 2 commits.
|(use "git push" to publish your local commits)
|nothing to commit, working tree clean

## And back to the start of this lesson
When you do the second push, you still have a race condition and may end up with a merge conflict. Keep at it until you succeed.

[Onward to further development with the team - Path 4 Lesson 5](./0405_team_dev.md)

* * *

#### When editing this page

Use [Dillinger](http://dillinger.io/) to test markdown. 






